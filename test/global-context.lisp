(in-package #:clpm-test)

(para:define-test global-context-editable-install
  (uiop:with-current-directory ((asdf:system-relative-pathname :clpm "test/global-context/editable-install/"))
    (with-clpm-env (:config-dir :global :data-dir data-dir)
      (clpm '("sync" "-VV")
            :output :interactive
            :error-output :interactive)
      (clpm '("install" "-y" "--asd" "test.asd")
            :output :interactive
            :error-output :interactive)
      (clpm '("exec"
              "--"
              "sbcl" "--non-interactive" "--no-userinit" "--no-sysinit"
              "--eval" "(require :asdf)"
              "--eval" "(asdf:load-system :test)"
              "--quit")
            :output :interactive
            :error-output :interactive))))

(para:define-test global-context-editable-install-with-client
  (uiop:with-current-directory ((asdf:system-relative-pathname :clpm "test/global-context/editable-install/"))
    (let ((*default-pathname-defaults* (uiop:getcwd)))
      (with-clpm-env (:config-dir :global :data-dir data-dir)
        (clpm-client:sync)
        ;; Ensure alexandria is installed as well.
        (flet ((validate (diff)
                 (para:true (member "alexandria" (clpm-client:context-diff-release-diffs diff)
                                    :test #'equal
                                    :key 'clpm-client:release-diff-project-name))
                 t))
          (clpm-client:install :asds '("test.asd") :validate #'validate))))))
