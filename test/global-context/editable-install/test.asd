(defsystem "test"
  :description "test system"
  :depends-on ("alexandria")
  :components ((:file "package")
               (:file "test")))
