(in-package #:clpm-test)

(defparameter *mkdtemp-random-bits* (* 8 4))

(defun mkdtemp (prefix &key (directory (uiop:temporary-directory)))
  "Make a temporary directory and return its pathname. This is not a secure
implementation, but it is portable."
  (loop
    :for suffix := (random (ash 1 *mkdtemp-random-bits*))
    :for name := (format nil "~A~X" prefix suffix)
    :for pn := (uiop:ensure-directory-pathname (merge-pathnames name directory))
    :do
       (multiple-value-bind (pathspec createdp)
           (ensure-directories-exist pn :mode #o700 :allow-other-keys t)
         (declare (ignore pathspec))
         (when createdp
           (return pn)))))

(defun call-with-temporary-directory (thunk &key (prefix "clpm-test-")
                                              (directory (uiop:temporary-directory)))
  (let ((dir (mkdtemp prefix :directory directory)))
    (unwind-protect
         (funcall thunk dir)
      (uiop:delete-directory-tree dir :validate t))))

(defmacro with-temporary-directory ((var &rest args
                                     &key prefix
                                       directory)
                                    &body body)
  (declare (ignore prefix directory))
  `(call-with-temporary-directory
    (lambda (,var)
      ,@body)
    ,@args))

#+sbcl
(defun posix-environment-alist ()
  "Returns an alist representing the environment variables."
  (let ((out nil))
    (dolist (pair-string (sb-ext:posix-environ))
      (let* ((pos-of-= (position #\= pair-string))
             (name (subseq pair-string 0 pos-of-=))
             (value (subseq pair-string (1+ pos-of-=))))
        (push (cons name value) out)))
    (nreverse out)))

#-sbcl
(defun posix-environment-alist ()
  "Returns an alist representing the environemnt variables."
  (error "Not implemented"))

#+sbcl
(defun run-program-augment-env-args (new-env-alist)
  "Given an alist of environment variables, return a list of arguments suitable
for ~uiop:{launch/run}-program~ to set the augmented environment for the child
process."
  (let ((env (posix-environment-alist)))
    (dolist (pair new-env-alist)
      (destructuring-bind (name . value) pair
        (setf (alex:assoc-value env name :test #'equal) value)))
    (list :environment
          (mapcar (lambda (c)
                    (concatenate 'string (car c) "=" (cdr c)))
                  env))))

#+ccl
(defun run-program-augment-env-args (new-env-alist)
  (list :env new-env-alist))

#-(or sbcl ccl)
(defun run-program-augment-env-args (new-env-alist)
  (error "not implemented"))
